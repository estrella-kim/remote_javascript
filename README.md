# LazySide JS Convention

- - -

LazySide Front-End 개발자들의 일관성 있는 코딩 스타일을 위한 규칙입니다.

1. 기본 규칙
2. 에디터 설정
3. js디렉토리
4. javscript
    1. 구문
    2. 들여쓰기
    3. 문장의 종료
    4. 명명규칙 camelCase
    5. 전역변수
5. Reference

- - -

## 기본 규칙
1. ES5/6 문법과 LazySide 코딩 가이드라인을 지켜 코드를 작성합니다.
2. 팀원들과 논의하여 규칙을 업데이트 할 수 있습니다.

- - -

## 에디터 설정

규칙을 준수하기 위해 에디터 환경을 설정해 둡니다.

- 들여쓰기는 공백문자 4개로 합니다.
- 파일 저장 시 줄 끝 공백문자를 제거합니다.
- 파일의 맨 마지막은 줄바꿈으로 끝납니다.

- - -

### js디렉토리
> 1. js폴더 내에서 라이브러리폴더, 환경설정폴더, 모듈폴더, 상수폴더로 나눕니다.
> 2. 라이브러리폴더에는 jquery, requirejs, babel, es6, 등 프레임워크 혹은 라이브러리 파일이 있습니다.
> 3. config폴더 내에는 dependeny 설정을 가지고 있는 js파일이 있습니다.
> 4. constants폴더에는 항상 변하지 않는 전역스코프를 가진 변수들을 정의한 js파일이 들어갑니다.
> 5. module은 기능별로 나눈 js파일이 들어갑니다.


```
[ 예시 ]



[festival]
  │
  ├[view]
  │  ├ login.html
  │  ├ notice.html
  │  └ introduction.html
  ├[css]
  │  
  ├[js]
  │  ├ [library]
  │  ├ [config]
  │  ├ [constants]
  │  └ [module]
  ├[static]
  └ index.html


```

## JavaScript

### 구문
- 변수의 마지막에 콤마를 붙입니다.

```
    var foo= 1,
        bar= 2,
        baz= 3;

    var obj= {
        foo: 1,
        bar: 2,
        baz: 3
    }
```

- Space는 한 줄 띄지 않고 바로 붙입니다.( space 4번은 tab 1번입니다)
- 함수명 전에 한 칸 띄어주고 세미콜론은 각각 다른 줄에서 열어주고 닫아줍니다.

```
   function foo(){
       return "bar";
   }
```


> 참고
> [Popular Convention](http://sideeffect.kr/popularconvention#javascript)

### 들여쓰기
- 절대 Space와 Tab을 섞어 쓰지 않습니다.
- 프로젝트를 시작할 때, 반드시 Space와 Tab 둘 중 하나를 선택하여야 합니다.(단, space4번은 tab 1번입니다.)
- 들여쓰기에 대한 규칙이 합의되면 편집기의 들여쓰기 설정을 규칙대로 설정 해놓고 사용할 것을 권장합니다.

### 문장의 종료
- 한 줄에 하나의 문장만 허용하며, 문장의 종료 시에는 반드시 세미콜론(;)을 사용합니다.

> 참고

> [안티 패턴 - 문장의 끝은 세미콜론(;)을 사용하라](https://github.com/nhnent/fe.javascript/wiki/%EC%95%88%ED%8B%B0-%ED%8C%A8%ED%84%B4#%EB%AC%B8%EC%9E%A5%EC%9D%98-%EB%81%9D%EC%9D%80-%EC%84%B8%EB%AF%B8%EC%BD%9C%EB%A1%A0%EC%9D%84-%EC%82%AC%EC%9A%A9%ED%95%98%EB%9D%BC)

### 명명규칙 camelCase
- 네이밍은 의미있게 작성하되, 축약형을 사용하지 않습니다.
- 공백을 허용하지 않습니다.
- 상수는 대문자와 '_'를 사용합니다.
- 변수, 함수는 소문자 카멜표기법을 사용합니다.
    - 배열은 복수형 이름을 사용
    - 정규표현식은 'r'로 시작
    - 이벤트 핸들러는 'on'으로 시작
    - 반환 값이 불린인 함수는 'is'로 시작
    - private은 '_'로 시작
- 생성자는 대문자 카멜표기법을 사용합니다.

[예시]

```
    // 상수
    SYMBOLIC_CONSTANTS;

    // 변수, 함수
    dog

    // 배열
    dogs;

    // 정규표현식
    rDesc;

    // 이벤트 핸들러
    onClick;

    // 불린 반환 함수
    isAvailable;

    // private
    _privateVariableName;
    _privateFunctionName;

    // 생성자
    ConstructorName;
```

### 전역 변수
네임스페이스를 사용하여 전역변수를 최소화하며 암묵적인 전역변수는 절대로 사용하지 않습니다.

> 참고

> [안티 패턴 - 전역변수 대신 네임스페이스를 사용하라](https://github.com/nhnent/fe.javascript/wiki/%EC%95%88%ED%8B%B0-%ED%8C%A8%ED%84%B4#%EC%A0%84%EC%97%AD%EB%B3%80%EC%88%98-%EB%8C%80%EC%8B%A0-%EB%84%A4%EC%9E%84%EC%8A%A4%ED%8E%98%EC%9D%B4%EC%8A%A4%EB%A5%BC-%EC%82%AC%EC%9A%A9%ED%95%98%EB%9D%BC)

> [안티 패턴 - 변수 선언시 반드시 "var" 키워드를 사용하라](https://github.com/nhnent/fe.javascript/wiki/%EC%95%88%ED%8B%B0-%ED%8C%A8%ED%84%B4#%EB%B3%80%EC%88%98-%EC%84%A0%EC%96%B8%EC%8B%9C-%EB%B0%98%EB%93%9C%EC%8B%9C-var-%ED%82%A4%EC%9B%8C%EB%93%9C%EB%A5%BC-%EC%82%AC%EC%9A%A9%ED%95%98%EB%9D%BC-es5)

- - -

### 변수의 위치
> 1. 변수는 상단에 정의합니다.
> 2. 변수와 함수, 조건문 등은 한 줄 띄어쓰기로 가시성 좋도록 분리해줍니다.

```
var foo=;
// 한 줄 띄어쓰기
function goToilet(){

}

```

### 5. 주석
